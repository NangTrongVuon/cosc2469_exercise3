package main

import "testing"

func benchmarkKWaySort(way int, num int,  b *testing.B) {
	for n := 0; n < b.N; n++ {
		kWaySort(way, num)
	}
}
////
func Benchmark4WaySort100 (b *testing.B) { benchmarkKWaySort(4 , 100 , b) }
func Benchmark4WaySort1000 (b *testing.B) { benchmarkKWaySort( 4 , 1000 , b)}
func Benchmark4WaySort10000 (b *testing.B) { benchmarkKWaySort( 4 , 10000 , b)}
func Benchmark4WaySort100000 (b *testing.B) { benchmarkKWaySort( 4 , 100000 , b)}
func Benchmark8WaySort100 (b *testing.B) { benchmarkKWaySort( 8 , 100 ,b) }
func Benchmark8WaySort1000 (b *testing.B) { benchmarkKWaySort( 8 , 1000 ,b)}
func Benchmark8WaySort10000 (b *testing.B) { benchmarkKWaySort( 8 , 10000 ,b)}
func Benchmark8WaySort100000 (b *testing.B) { benchmarkKWaySort( 8 , 100000 ,b)}
func Benchmark16WaySort100 (b *testing.B) { benchmarkKWaySort( 16 , 100 ,b)}
func Benchmark16WaySort1000 (b *testing.B) { benchmarkKWaySort( 16 , 1000 ,b)}
func Benchmark16WaySort10000 (b *testing.B) { benchmarkKWaySort( 16 , 10000 ,b)}
func Benchmark16WaySort100000 (b *testing.B) { benchmarkKWaySort( 16 , 100000 ,b)}
func Benchmark32WaySort100 (b *testing.B) { benchmarkKWaySort( 32 , 100 ,b)}
func Benchmark32WaySort1000 (b *testing.B) { benchmarkKWaySort( 32 , 1000 ,b)}
func Benchmark32WaySort10000 (b *testing.B) { benchmarkKWaySort( 32 , 10000 ,b)}
func Benchmark32WaySort100000 (b *testing.B) { benchmarkKWaySort( 32 , 100000 ,b)}
func Benchmark64WaySort100 (b *testing.B) { benchmarkKWaySort( 64 , 100 ,b)}
func Benchmark64WaySort1000 (b *testing.B) { benchmarkKWaySort( 64 , 1000 ,b)}
func Benchmark64WaySort10000 (b *testing.B) { benchmarkKWaySort( 64 , 10000 ,b)}
func Benchmark64WaySort100000 (b *testing.B) { benchmarkKWaySort( 64 , 100000 ,b)}
//
