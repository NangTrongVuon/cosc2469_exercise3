package main

import (
	"fmt"
	"io"
	"os"
	"io/ioutil"
	"bytes"
	"strings"
)

func sortTempInput(tempInputArray []int, count int) {
	tempInputArray = mergeSort(tempInputArray)
	//fmt.Print(tempInputArray, "\n")
	tempFile, err := os.Create(fmt.Sprintf("temp_%d.txt", count))

	for i := 0; i < len(tempInputArray); i++ {
		tempFile.WriteString(fmt.Sprintf("%d \n", tempInputArray[i]))
	}

	if err != nil {
		fmt.Errorf("Error writing int to file")
	}

	tempInputArray = []int {}
	//count++

	//fmt.Printf("Way %d complete \n", count)
}

// helper from
// https://stackoverflow.com/questions/24562942/golang-how-do-i-determine-the-number-of-lines-in-a-file-efficiently

func lineCounter(r io.Reader) (int, error) {
	buf := make([]byte, 32*1024)
	count := 0
	lineSep := []byte{'\n'}

	for {
		c, err := r.Read(buf)
		count += bytes.Count(buf[:c], lineSep)

		switch {
		case err == io.EOF:
			return count, nil

		case err != nil:
			return count, err
		}
	}
}

func createInitialRuns(fileName string, way int) []int {
	integerFile, err := ioutil.ReadFile(fileName)

	if err != nil {
		fmt.Println(err)
	}

	integerString := string(integerFile)

	integerCount, _ := lineCounter(strings.NewReader(integerString))

	sizePerArray := integerCount / way

	leftover := integerCount % way

	var testInts []int
	var newInt int
	var count = 0

	file, _ := os.Open(fileName)

	for {

		_, err := fmt.Fscanf(file, "%d", &newInt)

		if err != nil {
			if err == io.EOF {
				//sortTempInput(testInts, count)
				//println("End of file reached")
				break
			}
		}

		testInts = append(testInts, newInt)

		if leftover > 0 {
			sizePerArray += leftover
			leftover = 0
		}

		if len(testInts) == sizePerArray {
			if leftover == 0 {
				sizePerArray = integerCount / way
			}
			sortTempInput(testInts, count)
			testInts = []int {}
			count++
		}
	}

	defer file.Close()


	return testInts
}

func externalSort(fileCount int) {

	files := [] *os.File{}
	var tempInput int
	var newHeapNode MinHeapNode

	for i := 0; i < fileCount; i++ {
		new_file, _ := os.Open(fmt.Sprintf("temp_%d.txt", i))
		files = append(files, new_file)
	}

	outputFile, err := os.Create("result.txt")

	if err != nil {
		println(err)
	}

	tempHeapArray := []MinHeapNode {}


	// initial run
	for i := 0; i < fileCount; i++ {

		_, err := fmt.Fscanln(files[i], &tempInput)

		newHeapNode.data = tempInput
		newHeapNode.origin = i

		if err != nil {
			fmt.Print(err)
		}

		tempHeapArray = append(tempHeapArray, newHeapNode)

	}

	newHeap := NewHeap(tempHeapArray)
	//newHeap.PrintHeap()

	var nextInput int
	var nextHeapNode MinHeapNode


	for newHeap.size > 0 {

		nextWrite := newHeap.deleteMin()
		newHeap.heapArray = newHeap.heapArray[0:newHeap.size]
		newHeap.size--


		outputFile.WriteString(fmt.Sprintf("%d \n", nextWrite.data))

		nextHeapNode.origin = nextWrite.origin


		_, err = fmt.Fscanln(files[nextHeapNode.origin], &nextInput)

		if err != nil {
			if err == io.EOF {
				files[nextHeapNode.origin].Close()

			}
		} else {
			nextHeapNode.data = nextInput
			newHeap.heapArray = append(newHeap.heapArray, nextHeapNode)
			newHeap.size++
			newHeap.FixHeapUp(newHeap.size)
		}

	}
	//


	outputFile.Close()

	for i := 0; i < fileCount; i ++ {
		os.Remove(files[i].Name())
	}
}

// wikipedia top down merge sort implementation
func mergeSort(array []int) []int {
	if len(array) <= 1 {
		//print("Already sorted")
		return array
	}

	var left []int
	var right []int

	for index, element := range array {
		if index < len(array) / 2 {
			left = append(left, element)
		} else {
			right = append(right, element)
		}
 	}

	left = mergeSort(left)
	right = mergeSort(right)

	return merge(left, right)
}

func merge(left, right []int) []int {
	var result []int

	for len(left) > 0 && len(right) > 0 {
		if left[0] <= right[0] {
			result = append(result, left[0])
			left = append(left[:0], left[1:]...)
		} else {
			result = append(result, right[0])
			right = append(right[:0], right[1:]...)
		}
	}

	for len(left) > 0 {
		result = append(result, left[0])
		left = append(left[:0], left[1:]...)
	}

	for len(right) > 0 {
		result = append(result, right[0])
		right = append(right[:0], right[1:]...)
	}

	return result
}

func kWaySort(way int, n int) {
	// read the big file
	// split it into k parts, starting from 0
	createInitialRuns(fmt.Sprintf("%d.txt", n), way)

	// sort each k part, and write them
	// merge all k parts together
	externalSort(way)

}

func main() {

	kWaySort(8, 100)


}
