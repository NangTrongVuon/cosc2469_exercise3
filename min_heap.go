package main

import (
	"fmt"
)

// shoddy minimum heap implementation
// based on http://interactivepython.org/runestone/static/pythonds/Trees/BinaryHeapImplementation.html

type MinHeapNode struct {
	data int
	origin int
}

type MinHeap struct {
	heapArray []MinHeapNode
	size int
}

func NewHeap(inputArray []MinHeapNode) MinHeap {
	minHeap := MinHeap{}
	//fmt.Println(inputArray)
	minHeap = minHeap.BuildHeap(inputArray)
	return minHeap
}

func (minHeap MinHeap) BuildHeap(inputArray []MinHeapNode) MinHeap {

	i := len(inputArray) / 2

	minHeap.size = len(inputArray)
	newHeap := []MinHeapNode{}
	newHeap = append(newHeap, MinHeapNode{0, 0})
	minHeap.heapArray = append(newHeap, inputArray...)


	for i > 0 {
		//fmt.Println(i)
		minHeap.FixHeapDown(i)
		i -= 1
	}

	return minHeap
}

// Unsure why does this not work
func (minHeap MinHeap) Insert(insertNode MinHeapNode) {
	//fmt.Println("inserting: ", insertNode)
	//fmt.Println(minHeap.heapArray)
	//minHeap.heapArray = append(minHeap.heapArray, insertNode)
	//fmt.Print(minHeap.heapArray)
	//minHeap.size++
	//minHeap.FixHeapUp(minHeap.size)

}

func (minHeap MinHeap) FixHeapUp(i int) {
	for i / 2 > 0 {
		if minHeap.heapArray[i].data < minHeap.heapArray[i / 2].data {
			minHeap.heapArray[i / 2], minHeap.heapArray[i] = minHeap.heapArray[i], minHeap.heapArray[i / 2]
		}

		i /= 2

	}
}

func (minHeap MinHeap) FixHeapDown(i int) {

	for (i * 2) <= minHeap.size {


		minChild := minHeap.MinChild(i)

		if minHeap.heapArray[i].data > minHeap.heapArray[minChild].data {
			minHeap.heapArray[i], minHeap.heapArray[minChild] = minHeap.heapArray[minChild], minHeap.heapArray[i]
			//fmt.Println(heap.data)
		}

		i = minChild
	}
}

func (minHeap MinHeap) MinChild(i int) int {
	if i * 2 + 1 > minHeap.size {
		return i * 2
	} else {
		if minHeap.heapArray[i * 2].data < minHeap.heapArray[i * 2 + 1].data {
			return i * 2
		} else {
			return i * 2 + 1
		}
	}

}

// Unsure why this doesn't work either
func (minHeap MinHeap) deleteMin() MinHeapNode {

	deletedElement := minHeap.heapArray[1]

	copy(minHeap.heapArray[1:], minHeap.heapArray[2:])
	minHeap.heapArray[minHeap.size] = MinHeapNode{}
	minHeap.heapArray = minHeap.heapArray[:minHeap.size]

	minHeap.size--

	minHeap.FixHeapDown(1)

	return deletedElement
}


func (minHeap MinHeap) PrintHeap() {
	fmt.Println(minHeap.heapArray)
}